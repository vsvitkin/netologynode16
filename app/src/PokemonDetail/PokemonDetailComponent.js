'use strict';

pokemonApp.component('pokemonDetail', {

    controller: function PokemonDetailCtrl(PokemonsService, $routeParams) {

      this.pokemonLoaded = false;

      this.pokemon = PokemonsService.get({
          pokemonId: $routeParams['pokemonId']
      }, (successResult)=> {
          this.notfoundError = false;
          this.pokemonLoaded = true;
          this.activeTab = 1;
          this.disableControlTab = true;
      }, (errorResult)=> {
          this.notfoundError = true;
          this.pokemonLoaded = true;
      });

        this.deletePokemon = (pokemonId)=>{

          this.pokemon.$delete({
              pokemonId: pokemonId
          }, (successResult)=>{
              this.deletionSuccess = true;
          }, (errorResult)=>{
              this.deletionError = true;
          });

      }

    },

    templateUrl: './src/PokemonDetail/PokemonDetail.html'

});
